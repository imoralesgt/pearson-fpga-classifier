----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: Ivan Morales
-- 
-- Create Date: 03/23/2023 03:16:28 PM
-- Design Name: 
-- Module Name: averageMeanDeviation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Computes Mean Average Deviation (absolute deviation)
-- 
-- Dependencies: Requires average value to be computed in an external block
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity averageMeanDeviation is
    Generic(
        DATA_BUS_WIDTH_IN : NATURAL := 16;
        DATA_BUS_WIDTH_OUT : NATURAL := 16;
        MAX_WINDOW_SIZE : NATURAL := 10
    );
    
    Port (
        -- ======================================================
        -- Control ports
        -- ======================================================
        clk : in std_logic;
        reset : in std_logic;
        
        -- Data ports
        -- Input stream
        writeEnable : in std_logic; 
        ready : out std_logic;

        -- Output stream
        dValidOut : out std_logic;
        readEnable : in std_logic;
        
        
        -- ======================================================
        -- DSP ports
        -- ======================================================       
        -- x[n]
        x_n : in std_logic_vector(DATA_BUS_WIDTH_IN - 1 downto 0);
        
        -- x mean value (sampled at middle window)
        x_avg : in std_logic_vector(DATA_BUS_WIDTH_IN - 1 downto 0);
        
        -- y[n] <-- Mean average deviation output
        y_n_out_mad : out std_logic_vector(DATA_BUS_WIDTH_OUT - 1 downto 0);
        
        
        -- Log2_N (window size) used for bit shifting in final division (1/N)
        log2_N : in std_logic_vector(MAX_WINDOW_SIZE - 1 downto 0)
                
    );
end averageMeanDeviation;

architecture Behavioral of averageMeanDeviation is
    
    -- Shift register to store the full window to compute mean average deviation
    type FIFO is array(0 to 2**MAX_WINDOW_SIZE - 1) of std_logic_vector(DATA_BUS_WIDTH_IN - 1 downto 0);
    
    signal inFifo : FIFO;
    
    
    signal windowLen : integer range 0 to 2**MAX_WINDOW_SIZE - 1;
    signal absoluteDifference : signed(DATA_BUS_WIDTH_IN - 1 downto 0);
    
     
    
    
    signal windowSum : signed(MAX_WINDOW_SIZE + 2 + DATA_BUS_WIDTH_IN - 1 downto 0);   
    signal outValue  : signed(windowSum'range);
      
begin

    -- Absolute difference computation \x - x_avg\
    absoluteDifference <= abs(signed(x_n) - signed(x_avg));

    -- Window length computation based on log_2(N) input
    windowLen <= 2**to_integer(unsigned(log2_N));  -- 2^5 = 32, 2^6 = 64, etc...

            -- Data input and \x-avg\ computation -> storage in shift regsiter
control:    process(clk)
            begin
                if rising_edge(clk) then
                    if reset = '1' then
                        inFifo <= (others => (others => '0'));
                        dValidOut <= '0';
                    else
                        dValidOut <= '1';
                        if(writeEnable = '1') then
                            inFifo(0) <= std_logic_vector(abs(signed(x_n) - signed(x_avg)));
                            for i in 1 to 2**MAX_WINDOW_SIZE - 1 loop
                                if(i < windowLen) then
                                    inFifo(i) <= inFifo(i - 1);
                                else
                                    inFifo(i) <= (others => '0');
                                end if;
                            end loop;
                        else
                            inFifo <= inFifo;
                        end if;
                    end if;
                end if;
            end process;
            
            
            -- Carry out the absolute value additions (full window) concurrently
addition:   process(inFifo)
                variable sum : signed(windowSum'range);
            begin
                sum := (others => '0');
                for i in 0 to 2**MAX_WINDOW_SIZE - 1 loop
                    sum := sum + signed(inFifo(i));
                end loop;
                windowSum <= sum;
            end process;            


    -- Output value computation
    outValue <= shift_right(windowSum, to_integer(unsigned(log2_N)));
    y_n_out_mad <= std_logic_vector(outValue(y_n_out_mad'range));


end Behavioral;

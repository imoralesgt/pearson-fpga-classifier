----------------------------------------------------------------------------------
-- Company: ICTP
-- Engineer: Ivan Morales
-- 
-- Create Date: 20.03.2023 09:56:00
-- Design Name: mAvg
-- Module Name: mAvg - Behavioral
-- Project Name: Simplified Pearson's correlation for online classification
-- Target Devices: Artix-7, Zynq-7020
-- Tool Versions: 
-- Description: Implements a moving average with variable length when coupled to
-- Xilinx's variable length shift register. Functionality based on:
-- https://surf-vhdl.com/how-to-implement-moving-average-in-vhdl/
--
-- y[n] = (1/N)*(y[n-1] + x[n] - x[n - N]) ; n >= N
--
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mAvg is
    Generic(
        DATA_BUS_WIDTH : integer := 16; 
        MAX_DELAY_BITS : integer := 10;
        LOG2_N_BUS_WIDTH : integer := 4 -- Max capable delay is 1024 (2^10). 2^(2^4) = 2^16 > 2^10   
    );


    Port(
        -- ======================================================
        -- Control ports
        -- ======================================================
        clk      : in std_logic;
        reset    : in std_logic;

        -- Input stream
        writeEnable : in std_logic; 
        ready : out std_logic;

        -- Output stream
        dValidOut : out std_logic;
        readEnable : in std_logic;

        -- ======================================================
        -- DSP ports
        -- ======================================================       
        -- x[n]
        x_n : in std_logic_vector(DATA_BUS_WIDTH - 1 downto 0);

        -- x[n-N]
        x_n_delayedN : in std_logic_vector(DATA_BUS_WIDTH - 1 downto 0);
              
        -- log2(N), being N the averaging window length
        log2_N : in std_logic_vector(LOG2_N_BUS_WIDTH - 1 downto 0);

        -- y[n]
        y_n_out : out std_logic_vector(DATA_BUS_WIDTH - 1 downto 0);
        
               
        -- N output: drives variable shift register (SR) window length
        -- Hard coded 10 bits, since SR out is not capable to handle more than 1024 samples
        nOut : out std_logic_vector(MAX_DELAY_BITS - 1 downto 0);
        
        --Half of value above, to sample moving average at half of the window
        nOut_2: out std_logic_vector(MAX_DELAY_BITS - 2 downto 0)

    );

end mAvg;


-- y[n] = y[n-1] + x[n] - x[n-N] ; n >= N
architecture Behavioral of mAvg is

    constant DATA_BUS_WIDTH_LARGE : integer := DATA_BUS_WIDTH + LOG2_N_BUS_WIDTH;

    -- Control signals
--    signal initState : std_logic;
--    signal initCounter : unsigned(2**LOG2_N_BUS_WIDTH - 1 downto 0);
--    signal initValue : signed(DATA_BUS_WIDTH_LARGE - 1 downto 0);
    
    -- Data signals
    signal yDelayed : std_logic_vector(DATA_BUS_WIDTH_LARGE - 1 downto 0); -- y[n-1]
    signal yOut : std_logic_vector(DATA_BUS_WIDTH_LARGE - 1 downto 0); -- y[n]
    signal shiftBits : std_logic_vector(LOG2_N_BUS_WIDTH - 1 downto 0); -- log2(N) for division
    signal nValue : std_logic_vector(2**LOG2_N_BUS_WIDTH - 1 downto 0);
    signal y_n_value : std_logic_vector(DATA_BUS_WIDTH_LARGE - 1 downto 0);

begin

-- Data stream control signals
proc_ctrl:  process(clk)
            begin
                if rising_edge(clk) then
                    if reset = '1' then
                        ready <= '0';
                        dValidOut <= '0';
                    else
                        ready <= '1';
                        dValidOut <= '1';
                    end if;
                end if;
            end process;


-- Computing y[n]-1 from y[n]
proc_y_1:   process(clk)
            begin
                if rising_edge(clk) then
                    if reset = '1' then
                        yDelayed <= (others => '0');
                    else
--                        if(writeEnable = '1' and readEnable = '1') then
                        if(writeEnable = '1') then
                            yDelayed <= yOut;
                        else
                            yDelayed <= yDelayed;
                        end if;
                    end if;
                end if;
            end process;
        
        
-- Computing y[n-1] + x[n] - x[n-N]
proc_yOut:  process(clk)
            begin
                if rising_edge(clk) then
                    if reset = '1' then
                        yOut <= (others => '0');
                    else
--                        if(writeEnable = '1' and readEnable = '1') then
                        if(writeEnable = '1') then
                            yOut <= std_logic_vector(signed(yDelayed) + signed(x_n) - signed(x_n_delayedN));
                        else
                            yOut <= yOut;
                        end if;
                    end if;
                end if;
            end process;
    
    -- Computing N output to drive variable length FIFO depth input
    -- Compensate 2 clock cycles for FIFO output and this block register delays
    nValue <= std_logic_vector(to_unsigned(2**to_integer(unsigned(log2_N)), 2**LOG2_N_BUS_WIDTH) - 2);
    nOut <= nValue(MAX_DELAY_BITS - 1 downto 0);
    
    -- Half of N-delay (N/2) 
    nOut_2 <= nValue(MAX_DELAY_BITS - 1 downto 1);
    
            
    -- Computing (1/N)*y[n]   
    y_n_value <= std_logic_vector(shift_right(signed(yOut), to_integer(unsigned(log2_N)))); 
    y_n_out <= y_n_value(DATA_BUS_WIDTH - 1 downto 0);
    
    
end Behavioral;




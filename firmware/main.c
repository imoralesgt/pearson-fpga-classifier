/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "pulse.h"
#include "comblock.h"
#include "sleep.h"

#define COMBLOCK_0 XPAR_COMBLOCK_0_AXIL_BASEADDR
#define COMBLOCK_0_RAM XPAR_COMBLOCK_0_AXIF_BASEADDR

#define CBREG_LOG2N_WLEN	CB_OREG0
#define CBREG_WRITE_EN		CB_OREG1

unsigned int fifoCount;


int main()
{
    init_platform();

    fifoCount = 0;

    xil_printf("Running now\n\r");

    //Keep everything on reset state until data is ready1
    cbWrite(COMBLOCK_0, CB_IFIFO_CONTROL, 0x01);
    cbWrite(COMBLOCK_0, CB_IFIFO_CONTROL, 0x00);

    //Moving average window length
//    cbWrite(COMBLOCK_0, CBREG_LOG2N_WLEN, 5); //2^5 = 32 samples moving average
    cbWrite(COMBLOCK_0, CBREG_LOG2N_WLEN, 6); //2^6 = 64 samples moving average

    //Write test waveform pulse shape to DP-RAM
//    cbWriteBulk(COMBLOCK_0_RAM, &PULSE_TRACE, 1024);

    cbWriteBulk(COMBLOCK_0_RAM, &PULSE_TRACE_SYNTH_B2, 65535*sizeof(short)/sizeof(int));

    xil_printf("Reading data\n\r");

    cbWrite(COMBLOCK_0, CBREG_WRITE_EN, 0x01); //Write data from RAM to processing block


	while(1){
		;
//		while(fifoCount < 1023){
//			fifoCount = (unsigned int)cbRead(COMBLOCK_0, (CB_IFIFO_STATUS >> 16));
//			if(fifoCount > 0)
//				xil_printf("cnt: %u, ", fifoCount);
//		}
//		cbWrite(COMBLOCK_0, CBREG_WRITE_EN, 0x00); //Stop sending data from RAM to processing block

//		xil_printf("\n\r");
//
//		xil_printf("FIFO Full\n\r");

//		while(fifoCount > 0){
//			cbRead(COMBLOCK_0, CB_IFIFO_VALUE);
//			fifoCount = (unsigned int)cbRead(COMBLOCK_0, (CB_IFIFO_STATUS >> 16));
//		}

	}




    cleanup_platform();
    return 0;
}
